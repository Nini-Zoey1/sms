from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import View
from django.conf import settings
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired

import re
from .models import User
from utilities.send_active_email import send_active_email


# Create your views here.

# /user/register
class RegisterView(View):
    """注册"""

    def get(self, request):
        """显示注册页面"""
        return render(request, 'user/../../templates/user/register.html')

    def post(self, request):
        """进行注册处理"""
        # 接收数据
        username = request.POST.get('user_name')
        password = request.POST.get('pwd')
        cpassword = request.POST.get('cpwd')
        email = request.POST.get('email')

        # 进行数据校验
        if not all([username, password, email]):
            # 数据不完整
            return render(request, 'user/../../templates/user/register.html', {'errmsg': '用户名、密码或邮箱输入不完整！'})

        # 校验邮箱
        if not re.match(r'^[a-z0-9][\w.\-]*@[a-z0-9\-]+(\.[a-z]{2,5}){1,2}$', email):
            return render(request, 'user/../../templates/user/register.html', {'errmsg': '邮箱格式不正确'})

        # 检验两次输入的密码是否一致
        if cpassword != password:
            return render(request, 'user/../../templates/user/register.html', {'errmsg': '两次输入的密码不一致'})

        # 校验用户名是否重复
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            # 用户名不存在，可用
            user = None
        if user:
            # 用户名存在
            return render(request, 'user/register.html', {'errmsg': '用户名已存在'})

        # 进行业务处理: 进行用户注册
        user = User.objects.create_user(username, email, password)
        # 激活字段，默认为没有激活
        user.is_active = 0
        user.save()
        # 发送激活邮件，包含激活连接： http：//127.0.0.1：8000/user/active/用户id
        # 激活连接中需要包含用户的身份信息，并且要把身份信息进行加密处理

        # 加密用户的身份信息，生成激活的token
        serializer = Serializer(settings.SECRET_KEY, 86400)
        info = {'confirm': user.id}
        token = serializer.dumps(info)
        token = token.decode('utf8')

        # 发邮件
        try:
            send_active_email(email, username, token)
            # 返回应答,跳转到首页
            return render(request, 'user/waiting_active.html')
        except Exception as e:
            return render(request, 'user/waiting_active.html')


class ActiveView(View):
    """用户激活"""

    def get(self, request, token):
        """进行用户激活"""
        # 解密获取用户信息
        serializer = Serializer(settings.SECRET_KEY, 86400)
        try:
            info = serializer.loads(token)
            # 待激活的用户id
            user_id = info['confirm']

            # 通过id获取用户信息
            user = User.objects.get(id=user_id)
            user.is_active = 1
            user.save()
            return render(request, 'user/success_active.html')

        except SignatureExpired as e:
            return HttpResponse('激活链接已过期')


#
class LoginView(View):
    """登陆"""

    def get(self, request):
        """ 显示登陆页面"""
        return render(request, 'user/login.html')

    def post(self, request):
        """登陆校验"""
        # 接收数据
        username = request.POST.get('user_name')
        password = request.POST.get('pwd')

        # 校验数据
        if not all([username, password]):
            # print(username, password)
            return render(request, 'user/login.html', {'errmsg': '用户名或密码输入不完整！'})

        """业务处理，登陆校验"""
        # 自动密码加密对比
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                # 用户已激活
                # 记录用户的登陆状态
                login(request, user)
                # 获取登陆后要跳转的地址
                # 默认跳转到首页
                next_url = request.POST.get('/next', reverse('message_template'))
                response = redirect(next_url)
                response.set_cookie('username', username, max_age=7 * 24 * 3600)
                return response
            else:
                # 加密用户的身份信息，生成激活的token
                serializer = Serializer(settings.SECRET_KEY, 86400)
                info = {'confirm': user.id}
                token = serializer.dumps(info)
                token = token.decode('utf8')

                # 发邮件
                try:
                    send_active_email(user.email, username, token)
                    return render(request, 'user/login.html', {'errmsg': '账户未激活，请提醒管理员后台登陆并激活你的账户!'})
                except Exception as e:
                    return render(request, 'user/login.html', {'errmsg': '账户未激活，请提醒管理员后台登陆并激活你的账户!'})
        else:
            return render(request, 'user/login.html', {'errmsg': '用户名或密码错误'})


# user/logout
class LougoutView(View):
    """退出登陆"""

    def get(self, request):
        # 清除用户的session信息
        logout(request)
        # 跳转到登陆页面
        return redirect(reverse('login'))
