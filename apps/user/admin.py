from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User


# Register your models here.
class MyUserAdmin(UserAdmin):
    # 显示在管理页面的字段
    list_display = ('username', 'email', 'is_active', 'is_superuser')


# admin.site.unregister(User)
admin.site.register(User, MyUserAdmin)
