from django.core.paginator import Paginator
from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic import View

from .models import Driver


class DriverView(View):
    """司机列表"""

    def get(self, request):
        try:
            drivers = Driver.objects.all().order_by('id')
        except:
            """当前没有司机"""
            return render(request, 'driver/driver_list.html', {'errmsg': '当前司机列表为空！'})
        context = {
            'drivers': drivers
        }

        # 使用模版
        return render(request, 'driver/driver_list.html', context)


# /driver/<int:driver_id>
class DriverDetailView(View):
    """司机详情"""

    def get(self, request, driver_id):
        try:
            driver_id = int(driver_id)
            driver = Driver.objects.get(id=driver_id)
            context = {
                'driver': driver
            }
            return render(request, 'driver/driver_detail.html', context)
        except Exception as e:
            return redirect('/driver/driver_list/')
            # return render(request, 'driver/driver_list.html', {'errmsg': "找不到这个司机！"})

    def post(self, request, driver_id):
        """更改司机信息"""
        name = request.POST.get('name')
        phone = request.POST.get('phone')
        wechat = request.POST.get('wechat')
        plate_number = request.POST.get('plate_number')
        car_type = request.POST.get('type')
        color = request.POST.get('color')
        driver_id = int(driver_id)
        driver = Driver.objects.get(id=driver_id)
        context = {
            'driver': driver
        }
        if not all([name, phone, wechat, plate_number, car_type, color]):
            context['errmsg'] = '数据不完整'
            return render(request, 'driver/driver_detail.html', context)

        driver_id = int(driver_id)
        driver = Driver.objects.get(id=driver_id)
        driver.name = name
        driver.phone = phone
        driver.wechat = wechat
        driver.plate_number = plate_number
        driver.car_type = car_type
        driver.color = color
        try:
            driver.save()
            # print(driver.id)
            context = {
                'driver': driver
            }
            return redirect('driver_detail', driver_id=driver.id)
        # return render(request, 'driver/driver_detail.html', context)

        except Exception as e:
            context = {
                'driver': driver,
                'errmsg': e
            }
            return render(request, 'driver/driver_detail.html', context)
        # return render(request, 'driver/driver_detail.html', context)


# /driver/add_new/
class AddNewDriverView(View):
    """添加新司机"""

    def get(self, request):
        return render(request, 'driver/add_driver.html')

    def post(self, request):
        name = request.POST.get('name')
        phone = request.POST.get('phone')
        wechat = request.POST.get('wechat')
        plate_number = request.POST.get('plate_number')
        car_type = request.POST.get('type')
        color = request.POST.get('color')
        context = {
            'name': name,
            'phone': phone,
            'wechat': wechat,
            'plate_number': plate_number,
            'type': car_type,
            'color': color,
        }
        if not all([name, phone, wechat, plate_number, car_type, color]):
            context['errmsg'] = '数据不完整'
            return render(request, 'driver/add_driver.html', context)
        driver = Driver()
        driver.name = name
        driver.phone = phone
        driver.wechat = wechat
        driver.plate_number = plate_number
        driver.car_type = car_type
        driver.color = color
        try:
            driver.save()
            return redirect('driver_detail', driver_id=driver.id)
        # return render(request, 'driver/driver_detail.html', context)

        except Exception as e:
            context['errmsg'] = e
            return render(request, 'driver/add_driver.html', context)


# /driver/driver_id/delete
class DeleteDriverView(View):
    def get(self, request, driver_id):
        driver_id = int(driver_id)
        Driver.objects.filter(id=driver_id).delete()
        return redirect('driver_list')

# TODO:
#  1. 司机列表可点开查看司机详情  -----DONE
#  2. 司机列表页可以新增司机  -----DONE
#  3. 司机详情页更改或删除司机  -----DONE
#  4. 查询使用redirect  -----DONE

