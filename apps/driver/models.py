from django.db import models
from db.base_model import BaseModel


# Create your models here.
class Driver(BaseModel):

    name = models.CharField(max_length=50, verbose_name='司机姓名')
    phone = models.CharField(max_length=9, unique=True, verbose_name='司机电话(+61)')
    wechat = models.CharField(max_length=50, unique=True, verbose_name='司机微信号')
    plate_number = models.CharField(max_length=10, unique=True, verbose_name='车牌号')
    car_type = models.CharField(max_length=30, default='轿车', verbose_name='车型')
    color = models.CharField(max_length=50, default='混合', verbose_name='颜色')

    class Meta:
        db_table = 'sms_driver'
        verbose_name = '司机'
        verbose_name_plural = verbose_name
