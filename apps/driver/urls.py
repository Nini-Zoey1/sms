"""sms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from .views import DriverView, DriverDetailView, AddNewDriverView, DeleteDriverView
from django.contrib.auth.decorators import login_required

urlpatterns = [
    # url(r'^driver_list/(?P<page>\d+)$', login_required(DriverView.as_view()), name='driver_list'),
    path('driver_list/', login_required(DriverView.as_view()), name='driver_list'),
    path('<int:driver_id>/', login_required(DriverDetailView.as_view()), name='driver_detail'),
    path('add_new/', login_required(AddNewDriverView.as_view()), name='add_new'),
    path('<int:driver_id>/delete/', login_required(DeleteDriverView.as_view()), name='delete_driver'),
]
