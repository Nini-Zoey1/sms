from django.contrib import admin
from .models import Driver


# Register your models here.

# Register your models here.
class MyDriverAdmin(admin.ModelAdmin):
    # 显示在管理页面的字段
    list_display = ('id', 'name', 'phone', 'car_type')


# admin.site.unregister(User)
admin.site.register(Driver, MyDriverAdmin)
