"""sms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import MessageTemplateView, SendCNDropOffToCN, SendCNDropOffToAU, SendCNPickUpToCN, SendCNPickUpToAU, SendCoronavirusToCN, SendCoronavirusToAU
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('message_template/', login_required(MessageTemplateView.as_view()), name='message_template'),
    path('SMS_182678662/', login_required(SendCNDropOffToCN.as_view()), name='send_cn_drop_off_to_cn'),
    path('SMS_182673763/', login_required(SendCNDropOffToAU.as_view()), name='send_cn_drop_off_to_au'),
    path('SMS_182668833/', login_required(SendCNPickUpToCN.as_view()), name='send_cn_pick_up_to_cn'),
    path('SMS_182668835/', login_required(SendCNPickUpToAU.as_view()), name='send_cn_pick_up_to_au'),
    path('SMS_182679357/', login_required(SendCoronavirusToCN.as_view()), name='send_coronavirus_to_cn'),
    path('SMS_182674413/', login_required(SendCoronavirusToAU.as_view()), name='send_coronavirus_to_au'),
    # path('<string:template_code>/', SendCNDropOffToCN.as_view(), name='send_cn_airport_drop_off'),
]
