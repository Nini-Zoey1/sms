from django.core.paginator import Paginator
from django.shortcuts import render

# Create your views here.
from django.views.generic import View
from .models import MessageTemplate
from utilities.send_sms import send_cn_drop_off_to_au, send_cn_drop_off_to_cn, send_cn_pick_up_to_au, \
    send_cn_pick_up_to_cn, send_coronavirus_to_cn, send_coronavirus_to_au

import ast


# /message/message_templates
class MessageTemplateView(View):
    """信息模版列表页"""

    def get(self, request):
        try:
            templates = MessageTemplate.objects.all().order_by('id')
        except:
            """当前没有消息模板"""
            return render(request, 'message/message_template.html')

        # 组织模版上下文
        context = {
            'templates': templates,
        }
        # 使用模版
        return render(request, 'message/message_template.html', context)


# /message/SMS_182668833/
class SendCNPickUpToCN(View):
    """发送中文接机短信到中国号码"""

    def get(self, request):
        return render(request, 'message/SMS_182668833.html')

    def post(self, request):
        client_phone = request.POST.get('client_phone')
        name = request.POST.get('driver_name')
        plateNo = request.POST.get('plate_number')
        color = request.POST.get('color')
        type = request.POST.get('type')
        flight = request.POST.get('flight')
        airport = request.POST.get('start_address')
        end_addr = request.POST.get('end_address')

        if not all([client_phone, name, plateNo, color, type, flight, airport, end_addr]):
            return render(request, 'message/SMS_182668833.html', {'errmsg': "数据不完整！"})
        template_param = {
            'name': name,
            'plateNo': plateNo,
            'color': color,
            'type': type,
            'flight': flight,
            'airport': airport,
            'end_addr': end_addr
        }
        result = send_cn_pick_up_to_cn(client_phone, template_param)
        result = ast.literal_eval(result)
        if result['Message'] == 'OK':
            return render(request, 'message/SMS_182668833.html', {'errmsg': '发送成功!'})
        else:
            return render(request, 'message/SMS_182668833.html', {'errmsg': "发送失败，请重新尝试！" + str(result)})


# /message/SMS_182668835/
class SendCNPickUpToAU(View):
    """发送中文接机短信到澳洲号码"""

    def get(self, request):
        return render(request, 'message/SMS_182668835.html')

    def post(self, request):
        client_phone = request.POST.get('client_phone')
        name = request.POST.get('driver_name')
        plateNo = request.POST.get('plate_number')
        color = request.POST.get('color')
        type = request.POST.get('type')
        flight = request.POST.get('flight')
        airport = request.POST.get('start_address')
        end_addr = request.POST.get('end_address')
        contact = request.POST.get('contact')
        if not all([client_phone, name, plateNo, color, type, flight, airport, end_addr, contact]):
            return render(request, 'message/SMS_182668835.html', {'errmsg': "数据不完整！"})
        if client_phone[0] == '0':
            client_phone = '61' + client_phone[1:]
        else:
            client_phone = '61' + client_phone
        if contact[0] == '0':
            contact = '+61' + contact[1:]
        else:
            contact = '+61' + contact
        template_param = {
            'name': name,
            'plateNo': plateNo,
            'color': color,
            'type': type,
            'flight': flight,
            'airport': airport,
            'end_addr': end_addr,
            'contact': contact
        }
        result = send_cn_pick_up_to_au(client_phone, template_param)
        result = ast.literal_eval(result)
        if result['Message'] == 'OK':
            return render(request, 'message/SMS_182668835.html', {'errmsg': '发送成功!'})
        else:
            return render(request, 'message/SMS_182668835.html', {'errmsg': "发送失败，请重新尝试！" + str(result)})


# /message/SMS_182673763/
class SendCNDropOffToAU(View):
    """发送中文送机短信到澳洲号码"""

    def get(self, request):
        return render(request, 'message/SMS_182673763.html')

    def post(self, request):
        client_phone = request.POST.get('client_phone')
        driver_name = request.POST.get('driver_name')
        plate_number = request.POST.get('plate_number')
        color = request.POST.get('color')
        type = request.POST.get('type')
        date = request.POST.get('date')
        time = request.POST.get('time')
        time = date + ' ' + time
        start_address = request.POST.get('start_address')
        airport = request.POST.get('end_address')
        contact = request.POST.get('contact')
        if not all([client_phone, driver_name, plate_number, color, type, time, start_address, airport, contact]):
            return render(request, 'message/SMS_182673763.html', {'errmsg': '数据不完整'})

        if client_phone[0] == '0':
            client_phone = '61' + client_phone[1:]
        else:
            client_phone = '61' + client_phone
        if contact[0] == '0':
            contact = '+61' + contact[1:]
        else:
            contact = '+61' + contact
        template_param = {"name": driver_name,
                          "plateNo": plate_number,
                          "color": color,
                          "type": type,
                          "time": time,
                          "start_addr": start_address,
                          "airport": airport,
                          "contact": contact}
        result = send_cn_drop_off_to_au(client_phone, template_param)
        result = ast.literal_eval(result)
        if result['Message'] == 'OK':
            return render(request, 'message/SMS_182673763.html', {'errmsg': '发送成功!'})
        else:
            return render(request, 'message/SMS_182673763.html', {'errmsg': "发送失败，请重新尝试！" + str(result)})


# /message/SMS_182678662/
class SendCNDropOffToCN(View):
    """发送中文送机短信到国内号码"""

    def get(self, request):
        return render(request, 'message/SMS_182678662.html')

    def post(self, request):
        client_phone = request.POST.get('client_phone')
        driver_name = request.POST.get('driver_name')
        plate_number = request.POST.get('plate_number')
        color = request.POST.get('color')
        type = request.POST.get('type')
        date = request.POST.get('date')
        time = request.POST.get('time')
        time = date + ' ' + time
        start_address = request.POST.get('start_address')
        airport = request.POST.get('end_address')
        if not all([client_phone, driver_name, plate_number, color, type, time, start_address, airport]):
            return render(request, 'message/SMS_182678662.html', {'errmsg': '数据不完整'})

        template_param = {"name": driver_name,
                          "plateNo": plate_number,
                          "color": color,
                          "type": type,
                          "time": time,
                          "start_addr": start_address,
                          "airport": airport}
        result = send_cn_drop_off_to_cn(client_phone, template_param)
        result = ast.literal_eval(result)
        if result['Message'] == 'OK':
            return render(request, 'message/SMS_182678662.html', {'errmsg': '发送成功!'})
        else:
            return render(request, 'message/SMS_182678662.html', {'errmsg': "发送失败，请重新尝试！" + str(result)})


# /message/SMS_182679357/
class SendCoronavirusToCN(View):
    def get(self, request):
        return render(request, 'message/SMS_182679357.html')

    def post(self, request):
        client_phone = request.POST.get('client_phone')
        if not client_phone:
            return render(request, 'message/SMS_182679357.html', {'errmsg': '数据不完整'})
        result = send_coronavirus_to_cn(client_phone)
        result = ast.literal_eval(result)
        if result['Message'] == 'OK':
            return render(request, 'message/SMS_182679357.html', {'errmsg': '发送成功!'})
        else:
            return render(request, 'message/SMS_182679357.html', {'errmsg': "发送失败，请重新尝试！" + str(result)})
        # return render(request, 'message/SMS_182679357.html')


# /message/SMS_182674413/
class SendCoronavirusToAU(View):
    def get(self, request):
        return render(request, 'message/SMS_182674413.html')

    def post(self, request):
        client_phone = request.POST.get('client_phone')
        if client_phone[0] == '0':
            client_phone = '61' + client_phone[1:]
        else:
            client_phone = '61' + client_phone
        if not client_phone:
            return render(request, 'message/SMS_182674413.html', {'errmsg': '数据不完整'})
        result = send_coronavirus_to_au(client_phone)
        result = ast.literal_eval(result)
        if result['Message'] == 'OK':
            return render(request, 'message/SMS_182674413.html', {'errmsg': '发送成功!'})
        else:
            return render(request, 'message/SMS_182674413.html', {'errmsg': "发送失败，请重新尝试！" + str(result)})
