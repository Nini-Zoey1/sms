from django.contrib import admin
from .models import Message, MessageTemplate


# Register your models here.

class MyMessageTemplateAdmin(admin.ModelAdmin):
    # 显示在管理页面的字段
    list_display = ('template_code', 'template_name', 'template_sign')


# admin.site.unregister(User)
admin.site.register(MessageTemplate, MyMessageTemplateAdmin)
# admin.site.register(Message)
