from django.db import models
from db.base_model import BaseModel

from apps.driver.models import Driver


# Create your models here.

class MessageTemplate(BaseModel):
    template_types = (
        (0, 'verify_code'),
        (1, 'notification'),
        (2, 'advertise'),
        (3, 'international'),
    )
    template_code = models.CharField(max_length=50, verbose_name='模板代码')
    template_name = models.CharField(max_length=50, verbose_name='模板名称')
    create_date = models.DateTimeField(blank=True, null=True, verbose_name='模板创建时间')
    template_content = models.CharField(max_length=255, verbose_name='模板内容')
    template_type = models.SmallIntegerField(default=1, choices=template_types, verbose_name='模板类型')
    template_sign = models.CharField(max_length=30, verbose_name='模版签名')

    class Meta:
        db_table = 'sms_message_template'
        verbose_name = '短信模板'
        verbose_name_plural = verbose_name


class Message(BaseModel):
    message_types = (
        (0, 'airport_pickup'),
        (1, 'airport_dropoff'),
    )
    client_language_choices = (
        (0, 'Mandarin'),
        (1, 'English'),
    )
    water_requirements = (
        (0, 'No'),
        (1, 'Yes'),

    )
    special_requirements = (
        (0, 'No'),
        (1, 'Yes'),

    )
    driver_id = models.ForeignKey(Driver, related_name='司机id', on_delete=models.SET_NULL, null=True, blank=True)
    time = models.DateTimeField(null=True, blank=True, verbose_name='出行时间')
    message_type = models.SmallIntegerField(default=0, choices=message_types, verbose_name='服务类型')
    start_address = models.CharField(max_length=255, verbose_name='始发地')
    end_address = models.CharField(max_length=255, verbose_name='目的地')
    client_name = models.CharField(max_length=50, verbose_name='乘客名字')
    client_phone = models.CharField(max_length=11, verbose_name='乘客电话')
    client_language = models.SmallIntegerField(default=0, choices=client_language_choices, verbose_name='乘客语言')
    number_of_clients = models.SmallIntegerField(default=1, verbose_name='乘客数量')
    flight_number = models.CharField(max_length=20, null=True, blank=True, verbose_name='航班号')
    order_id = models.CharField(max_length=30, verbose_name='订单号')
    water_requirement = models.SmallIntegerField(default=0, choices=water_requirements, verbose_name='提供矿泉水')
    special_requirement = models.SmallIntegerField(default=0, choices=special_requirements, verbose_name='是否提供矿泉水')
    distance = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='订单公里数')
    total_price = models.DecimalField(max_digits=6, decimal_places=2, verbose_name='订单结算价')

    class Meta:
        db_table = 'sms_message'
        verbose_name = '信息详情'
        verbose_name_plural = verbose_name
