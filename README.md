SMS Notifications Send Platform
===

* It is a platform to send regularly company SMS notifications.
* It is a closure platform, which means it can only been used for the authorited users.

![register](screenshots/0.png)

![login](screenshots/1.png)

![sms templates](screenshots/2.png)

![example of using template](screenshots/3.png)

![driver information](screenshots/4.png)