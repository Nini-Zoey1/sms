from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.request import CommonRequest

client = AcsClient('LTAI4FuEwGodjzVwKeMByR5E',
                   'IZXqSjSxjaQ3mlCGbsQdqQlH3pt02n', 'cn-hangzhou')


def send_cn_drop_off_to_cn(client_phone, template_param):
    request = CommonRequest()
    request.set_accept_format('json')
    request.set_domain('dysmsapi.aliyuncs.com')
    request.set_method('POST')
    request.set_protocol_type('https')  # https | http
    request.set_version('2017-05-25')
    request.set_action_name('SendSms')

    request.add_query_param('RegionId', "cn-hangzhou")
    request.add_query_param('PhoneNumbers', client_phone)
    request.add_query_param('SignName', "安广澳洲服务")
    request.add_query_param('TemplateCode', "SMS_182678662")
    request.add_query_param(
        'TemplateParam', template_param)

    response = client.do_action(request)
    # python2:  print(response)
    # print(str(response, encoding='utf-8'))
    return str(response, encoding='utf-8')


def send_cn_pick_up_to_cn(client_phone, template_param):
    request = CommonRequest()
    request.set_accept_format('json')
    request.set_domain('dysmsapi.aliyuncs.com')
    request.set_method('POST')
    request.set_protocol_type('https')  # https | http
    request.set_version('2017-05-25')
    request.set_action_name('SendSms')

    request.add_query_param('RegionId', "cn-hangzhou")
    request.add_query_param('PhoneNumbers', client_phone)
    request.add_query_param('SignName', "安广澳洲服务")
    request.add_query_param('TemplateCode', "SMS_182668833")
    request.add_query_param(
        'TemplateParam', template_param)

    response = client.do_action(request)
    # python2:  print(response)
    # print(str(response, encoding='utf-8'))
    return str(response, encoding='utf-8')


def send_cn_drop_off_to_au(client_phone, template_param):
    request = CommonRequest()
    request.set_accept_format('json')
    request.set_domain('dysmsapi.aliyuncs.com')
    request.set_method('POST')
    request.set_protocol_type('https')  # https | http
    request.set_version('2017-05-25')
    request.set_action_name('SendSms')

    request.add_query_param('RegionId', "cn-hangzhou")
    request.add_query_param('PhoneNumbers', client_phone)
    request.add_query_param('SignName', "安广澳洲服务")
    request.add_query_param('TemplateCode', "SMS_182673763")
    request.add_query_param(
        'TemplateParam', template_param)

    response = client.do_action(request)
    # python2:  print(response)
    # print(str(response, encoding='utf-8'))
    return str(response, encoding='utf-8')


def send_cn_pick_up_to_au(client_phone, template_param):
    request = CommonRequest()
    request.set_accept_format('json')
    request.set_domain('dysmsapi.aliyuncs.com')
    request.set_method('POST')
    request.set_protocol_type('https')  # https | http
    request.set_version('2017-05-25')
    request.set_action_name('SendSms')

    request.add_query_param('RegionId', "cn-hangzhou")
    request.add_query_param('PhoneNumbers', client_phone)
    request.add_query_param('SignName', "安广澳洲服务")
    request.add_query_param('TemplateCode', "SMS_182668835")
    request.add_query_param(
        'TemplateParam', template_param)

    response = client.do_action(request)
    # python2:  print(response)
    # print(str(response, encoding='utf-8'))
    return str(response, encoding='utf-8')


def send_coronavirus_to_cn(client_phone):
    request = CommonRequest()
    request.set_accept_format('json')
    request.set_domain('dysmsapi.aliyuncs.com')
    request.set_method('POST')
    request.set_protocol_type('https')  # https | http
    request.set_version('2017-05-25')
    request.set_action_name('SendSms')

    request.add_query_param('RegionId', "cn-hangzhou")
    request.add_query_param('PhoneNumbers', client_phone)
    request.add_query_param('SignName', "安广澳洲服务")
    request.add_query_param('TemplateCode', "SMS_182679357")
    # request.add_query_param(
    #     'TemplateParam', template_param)

    response = client.do_action(request)
    # python2:  print(response)
    # print(str(response, encoding='utf-8'))
    return str(response, encoding='utf-8')


def send_coronavirus_to_au(client_phone):
    request = CommonRequest()
    request.set_accept_format('json')
    request.set_domain('dysmsapi.aliyuncs.com')
    request.set_method('POST')
    request.set_protocol_type('https')  # https | http
    request.set_version('2017-05-25')
    request.set_action_name('SendSms')

    request.add_query_param('RegionId', "cn-hangzhou")
    request.add_query_param('PhoneNumbers', client_phone)
    request.add_query_param('SignName', "安广澳洲服务")
    request.add_query_param('TemplateCode', "SMS_182674413")
    # request.add_query_param(
    #     'TemplateParam', template_param)

    response = client.do_action(request)
    # python2:  print(response)
    # print(str(response, encoding='utf-8'))
    return str(response, encoding='utf-8')

# send_cn_drop_off_to_cn('18606308091',
#                        "{\"name\":\"LI XU\",\"plateNumber\":\"asdfed\",\"carColor\":\"red\",\"carType\":\"suv\","
#                        "\"dateTime\":\"202001011025\",\"startAddr\":\"2 Muller Lane\",\"airportName\":\"Kingsford\","
#                        "\"phoneNumber\":\"61403285822\"}")

# send_coronavirus_to_cn('18435122609')
